# Analytics on UN Population

## Brief Description

In this poject we are provided with a raw data that consists of Population statistics of diffrent countries and region around the world. This data is provided to the United Nations Organization and can be downloaded via this link: <https://datahub.io/core/population-growth-estimates-and-projections/r/population-estimates.csv> Our aim in this project will be to convert raw open data, country and year wise population estimates in this case, into charts that tell some kind of story.

## Installation

These are some basic steps to follow for installation of the project

* Create a folder in your local machine and run the following command in the terminal set the path to that directory
```
$ git init
```
* Now click on the clone button on the repository page of the project(link: <>)
* Now click on clone via HTTPS option , an address will be copied to your clipboard
* Now run the following command in your machine's terminal and paste the address
```
$ git clone <paste the address here>
```
* All the project files and folders will be cloned to your local system inside the folder
* Through terminal you have to install two packages of python that is tkinter and matplotlib
Run the following commands to do so,
```
$ pip install tkinter

$ pip install matplotlib
```
* Next you can run the project main file that is main.py via running this command
```
$ python3 main.py
```
* In the terminal itself you will get four options of choosing the particular graph you want to see. The option will be from 1 to 4, you can press any option key and hit enter.
* You will be able to see the graph in a new window.

## Contributing

* Fork it!
* Create your feature branch: 
```
git checkout -b my-new-feature
```
* Commit your changes: 
```
git commit -am 'Add some feature'
```
* Push to the branch:
``` 
git push origin my-new-feature
```
* Submit a pull request :D
